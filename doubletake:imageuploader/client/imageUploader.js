if (Meteor.isClient) {
  var BinaryFileReader = {
    read: function (file, callback) {
      var reader = new FileReader;

      var fileInfo = {
        name: file.name,
        type: file.type,
        size: file.size,
        data: null
      };

      reader.onload = function () {
        fileInfo.data = reader.result;
        callback(null, fileInfo);
      };

      reader.onerror = function () {
        // if there's an error pass it as the first parameter to our
        // callback
        callback(reader.error);
      };

      // read the file into memory in a specified format like
      // readAsText, reasAsBinaryString, readAsDataURL,
      // readAsArrayBuffer
      reader.readAsDataURL(file);
    }
  };
    
var BinaryFileSave = {
    saveBinary: function(data,callback){    
            Images.insert(data); 
        
        
    }
}



Template.imageUpload.helpers({
    image : function(){
        return Images.findOne({user:Meteor.userId()});
    }
});

    

  Template.imageUpload.events({
      
    "click #upload": function (e, tmpl) {
        var fileInput = tmpl.find('input[type=file]');

      var fileList = fileInput.files;
      e.preventDefault();

      //for (var i = 0; i < fileList.length; i++) {

        BinaryFileReader.read(fileList[0], function (err, fileInfo) {
          if (err) {
            console.log(err);
          }
          else {

            if(fileInfo.type == 'image/jpeg'){
                var ext = '.jpg';
            }
            else if(fileInfo.type == 'image/png'){
                var ext = '.png';
            }
            var userId = Meteor.userId();
            var fileString = userId+'-'+ new Date().getTime() + ext;
            var url = '/app-storage/'+fileString;
              var data = {
                  src:fileInfo.data,
                  url: url,
                  fileName: fileString
              }
             
                 Meteor.call('uploadFile',data, function (error, result) { 
                        
                     if(!error){
                         $('.img-thumbnail').css('width','264px');
                         $('.img-thumbnail').css('height','auto');
                         $('.img-thumbnail').attr('src',data.src);
                         $('.img-thumbnail').data('filename',data.fileName);

                        }
                        

                });
                 
              
          }
        });
     // }
    }
  });
}