Package.describe({
  name: 'doubletake:imageuploader',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Image Uploader to Modulus.io Cloud',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/doubletake/imageuploader',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.0.3.1');
    api.use(['templating','mizzao:bootstrap-3','pcel:holder'])
    //templates must be added first
    api.addFiles('client/imageUploader.html',"client");
    api.addFiles('client/imageUploader.css',"client");
    //then other files
    api.addFiles('client/imageUploader.js',"client");
    api.addFiles('server/imageUploader.js',"server");
});

Package.onTest(function(api) {
  /*api.use('tinytest');
  api.use('doubletake:imageuploader');
  api.addFiles('doubletake:imageuploader-tests.js');*/
});
